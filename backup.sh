#!/bin/sh

set -e

echo "Job started: $(date)"

DATE=$(date +%Y%m%d_%H%M%S)
FILE="/db/backup/backup-$DATE.sql"
RESERVE_NUM=${RESERVES:-7}

# exec command
start=$(date +"%Y-%m-%d %H:%M:%S")
echo "$start: start database dump of $DATABASE..."

mkdir -p /db/backup
cd /db
sqlite3 $DATABASE .dump > $FILE

RET=$?
end=$(date +"%Y-%m-%d %H:%M:%S")

# check result and cleanup old file
if [ $RET -ne 0 ]; then
  echo "$end: backup error: exit code $RET"
  rm -rf /db/backup/$FILE
  exit $RET
else
  echo "$end: backup successful."
  cd /db/backup && ls -lrt | head -n -$RESERVE_NUM | awk '{print $9}' | xargs rm -f
  exit 0
fi

echo "Job finished: $(date)"
